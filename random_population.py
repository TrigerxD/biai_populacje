import random as rn
import sys
import os

population = sys.argv[1]
population_name = sys.argv[2]
rn.seed()

file = open('F:\\Studia\\Studia_6sem\\BIAI\\biai_populacje\\Params\\Populacje\\' + population_name + '.csv', 'w')

i = 0
file.write("rozmiar;masa;szybkosc;kondycja;wytrzymalosc;odpornosc na temperatury;futro;nielot/lot;srodowisko zycia;glod;typ pozywienia;plec\n")

while i < int(population):
	mass = rn.uniform(1.0,10.0)
	growth = rn.uniform(1.0,10.0)
	speed = rn.uniform(1.0,10.0)
	condition = rn.uniform(1.0,10.0)
	strenght = rn.uniform(1.0,10.0)
	temperature_resistance = rn.uniform(1.0,10.0)
	fuhr = rn.uniform(0.0,10.0)
	fly = rn.randint(0,1)
	life_place = rn.randint(0,2)
	if(fly == 1):
		life_place = 1
	hunger = rn.uniform(1.0,10.0)
	food_type = 0
	gender = rn.randint(0,1)
	file.write(str(growth)+";"+str(mass)+";"+str(speed)+";"+str(condition)+";"+str(strenght)+";"+str(temperature_resistance)+";"+str(fuhr)+";"+str(fly)+";"+str(life_place)+";"+str(hunger)+";"+str(food_type)+";"+str(gender)+'\n')
	i += 1
file.write("end;;;;;;;;;;;=SUM(L2:L"+str(int(population) + 1)+")\n")
file.close()