﻿#pragma once
#include "Species.h"
#include "Environmental_params.h"

class Population
{
	std::vector<Species> parameters;

public:
	Population();
	void read_file(std::string file_name);

	void death(int how_many);
	void death_by_cataclism(enum param_types type, int env);
	void sort();
	void sort_by_param(enum param_types type, int env);
	void set_nominal_value(int index, float value);
	void add(Species * fellow);
	void get_average();
	void grow();

	std::string * str_to_table(std::string x);	  // zwraca tabelę, dzieli wzgledem sredników
	void add_person(std::string * params);
	//void add_params(Species *obj, Enviroment::str_to_table(input));

	//opcja 1. Species * obj = new Species(Enviromental_params::str_to_table(input)); | parameters.push_back(Species(Enviromental_params::str_to_table(input)));
	//opcja 2. add_params(obj, Enviromental_params::str_to_table(input));

	//parameters.push_back(obj);
	//input.close();
	std::vector<Species> getPopulation();
};

