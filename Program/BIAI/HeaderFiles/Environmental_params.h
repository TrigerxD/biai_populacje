#pragma once
#include "Data.h"
#include <cstdio>
#include <ctime>

#define YEAR 366

class Environment {
	//std::vector<Data> parameters;
	Data parameters[YEAR];
	int field;
	double water;
	bool sorted;

public:
	Environment();

	void read_file(std::string file_name);
	void write_data(Calendar t_date, Data date, params_types type, double value);

	Data * get_day(Calendar date);
	Data * get_param(params_types type, Calendar date);
	Data * get_parameters();
	std::vector<std::string> get_params();
	int get_params_number();
	void sort();

	void set_field(std::string x);
	void set_water(double x);
	int get_field();
	double get_water();
};

int find_all(std::string x, char y);
std::string * str_to_table(std::string x);