#pragma once
#include <string>

class Calendar {
	int day;
	int month;
	int year;

	bool validate(int t_day, int t_month, int t_year);
	int day_month[12] = { 31,29,31,30,31,30,31,31,30,31,30,31 };
public:
	

	Calendar();
	Calendar(int t_day, int t_month, int t_year);
	Calendar(std::string date);
	Calendar(const Calendar &date);

	void set_day(int new_day);
	void set_month(int new_month);
	void set_year(int new_year);
	int get_day();
	int get_month();
	int get_year();
	int month_sum(int last_month);
	int get_day_month(int index);

	std::string get_date();

	bool operator==(Calendar& x);
	Calendar * operator=(Calendar& x);
};