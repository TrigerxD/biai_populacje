#pragma once
#include "Calendar.h"
#include <vector>
#include <iostream>
#include <fstream>

#define not_measure_value -999999

typedef struct Parameters {
	Calendar date;
	double temperature;
	double humidity;
	double cloudiness;
	double wind_speed;
	double pressure;
	double fall;
	double snow_height;
	Parameters * next;
	Parameters * prev;
} params;

enum params_types {
	temperature = 1,
	humidity,
	cloudiness,
	wind_speed,
	pressure,
	fall,
	snow_height
};

params_types convert(std::string x);

class Data {	
	int sortingVal;
	std::vector<params> first;
	int _params;
	int actual_param;
	params alloc_params(Calendar t_date);
public:
	Data();
	Data(Calendar t_date);
	Data(const Data &t_date);

	void add(Calendar t_date);
	int add(params_types type, double value, Calendar t_date);

	Calendar get_date(int index);
	//std::string get_params(Calendar t_date);
	int get_params_number();
	double get_average_param(params_types type, Calendar t_date);
	int get_sortingVal();
	void sort();
	bool is_first();
	void set_actual_param();

	Data * operator=(Data& x);
};