#pragma once
#include "Population.h"
#include <iostream>
#include <thread>
class Simulation {

public:
	Simulation();

	enum disaster_types {
		earthquake = 1,
		drought,
		tornado,
		volcano_erruption,
		flood
	};		

	void nominal_value_func(Environment * env,  Data params, Population &pop, Calendar t_date, Species * pattern);
	void mutate(Species * fellow);
	void evolve(Species * fellow);
	bool is_death_after_disaster(Population * pop);
	Species * reproduce(Species * male, Species * female);
	void death(Population * pop, float nominal_value);
	
};

float dependency_at_temperature(float actual_temperature, Species * fellow);
float dependency_at_wind_speed(float wind_speed, Species * fellow);
float dependency_at_cloudiness(float cloudiness, Species * fellow);
float dependency_at_fall(float fall, Species * fellow);
float dependency_at_pressure(float pressure, Species * fellow);
float dependency_at_humididty(float humidity, Species * fellow);
float dependency_at_snow_height(float snow_height, Species * fellow);
void task(Environment * env, Data params, Population &pop, Calendar t_date, int size, int start);