﻿#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>

#define MAX_EVOLVE 11
#define EVOLVE 2.5
#define MODIFICATE 0.25
#define MAX_GROW 10
#define MIN_VALUE 1

enum param_types {
	size = 0, // (1, 10)
	weight = 1,
	speed = 2,
	durability = 4,
	condition = 3,
	temp_resistance = 5,
	enviroment = 8,
	food_demand = 9,
	food_type = 10,
	fur = 6,
	flight = 7,
	gender = 11
};

class Species
{
private:
	double size;
	double weight; //napisać funckje addparameteres, ktorej parametry przyja wskaznik na obiekt i wskaznik na 
	double speed;
	double durability;
	double condition;
	double temp_resistance;
	double enviroment;
	double food_demand;
	double food_type;
	double fur;
	bool flight;
	bool gender;
	int lifetime;

	bool evolve;
	float nominal_value;
public:
	Species();
	Species(const Species &tmp);
	Species(float * params);

	void mutate();
	void evolving();
	void grow();
	Species * reproduce(Species * female);

	void set_nominal_value(float x);
	float get_nominal_value();

	void add_parameters(Species* obj);
	void set_size(double new_size);
	void set_weight(double new_weight);
	void set_speed(double new_speed);
	void set_durability(double new_durability);
	void set_condition(double new_condition);
	void set_temp_resistance(double new_temp_resistance);
	void set_enviroment(double new_enviroment);
	void set_food_demand(double new_food_demand);
	void set_food_type(double new_food_type);
	void set_fur(double new_fur);
	void set_flight(bool new_flight);
	void set_gender(bool new_gender);

	void increment_params();

	double get_size();
	double get_weight();
	double get_speed();
	double get_durability();
	double get_condition();
	double get_temp_resistance();
	double get_enviroment();
	double get_food_demand();
	double get_food_type();
	double get_fur();
	bool get_flight();
	bool get_gender();
	int get_lifetime();
};

