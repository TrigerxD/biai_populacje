﻿#include "../HeaderFiles/Environmental_params.h"

Environment::Environment() {
	field = 0;
	water = 0;
	sorted = false;
}

void Environment::read_file(std::string file_name) {
	std::fstream input;
	input.open(file_name);
	if(input.good()){
		std::string param_types = "";
		getline(input, param_types);
		int count = find_all(param_types, ';') + 1;

		std::string * table;
		table = str_to_table(param_types);

		std::string line;
		int i = 0;
		clock_t start = ((clock()) / CLOCKS_PER_SEC);
		for (int i = 0; i < YEAR; i++)
			this->parameters[i].set_actual_param();

		for(i;; i++) {
			getline(input, line);
			std::string * tmp = str_to_table(line);
			if (tmp[0] == "end")
				break;
			Calendar * date = new Calendar(tmp[0]);
			Data * tmp_data = new Data(*date);
			for (int j = 1; j < count; j++) {
				write_data(Calendar(*date), *tmp_data, convert(table[j]), stof(tmp[j]));
			}
			delete(tmp_data);
			delete(date);
		}
		std::cout << i << std::endl;
		std::cout << "Zapisano w : " << (clock()/CLOCKS_PER_SEC) - start << " s" << std::endl;
		input.close();
		start = ((clock()) / CLOCKS_PER_SEC);
		Environment::sort();
		std::cout << "Posortowano w : " << (clock() / CLOCKS_PER_SEC) - start << " s" << std::endl;
	}
}

void Environment::write_data(Calendar t_date, Data date, params_types type, double value) {
	int index = date.get_sortingVal() - 1;
	if(parameters[index].get_sortingVal() == 0)
		parameters[index] = date;
	
	if (parameters[index].add(type, value, t_date) == 1) {}
	else {
		parameters[index].add(t_date);
		parameters[index].add(type, value, t_date);
	}
}

Data * Environment::get_day(Calendar date) {
	return nullptr;
}

Data * Environment::get_param(params_types type, Calendar date) {
	return nullptr;
}

Data * Environment::get_parameters()
{
	return parameters;
}

std::vector<std::string> Environment::get_params() {
	std::vector<std::string> retVal;
	retVal.push_back("temperature;humidity;cloudiness;wind_speed;pressure;fall;snow_height");
	for (int i = 0; i < 366; i++) {
		if (parameters[i].get_sortingVal() != 0) {}
			/*retVal.push_back(parameters[i].get_params());*/
	}

	return retVal;
}

int Environment::get_params_number() {
	int retVal = 0;

	for (int i = 0; i < 366; i++) {
		if (parameters[i].get_sortingVal() != 0)
			retVal += parameters[i].get_params_number();
	}

	return retVal;
}

int find_all(std::string x, char y) {
	int retVal = 0;
	for (int i = 0; i < x.length(); i++) {
		if (x[i] == y)
			retVal++;
	}
	return retVal;
}

params_types convert(std::string x) {
	if (x == "temperature") return temperature;
	else if (x == "humidity") return humidity;
	else if (x == "cloudiness") return cloudiness;
	else if (x == "wind_speed") return wind_speed;
	else if (x == "pressure") return pressure;
	else if (x == "fall") return fall;
	else if (x == "snow_height") return snow_height;
}

std::string * str_to_table(std::string x) {
	std::string * retVal = new std::string[find_all(x, ';') + 1];
	int a = 0;
	for (int i = 0; i < x.length(); i++) {		
		if (x[i] == ';')
			a++;
		else if (x[i] == ',')
			retVal[a] += '.';
		else
			retVal[a] += x[i];
	}
	return retVal; //zwraca tabelę wszystkich elemetów (wiecej o 1 niz liczba sredniów
}

void Environment::sort() {
	for (int i = 0; i < YEAR; i++) {
		if(parameters[i].is_first())
			parameters[i].sort();
	}
	sorted = true;
}

void Environment::set_field(std::string x)
{
	if (x == "plain") {
		field = 1;
	}
	else if (x == "mountainous") {
		field = 5;
	}
	else if (x == "beach") {
		field = 3;
	}
	else
		field = 1;
}

void Environment::set_water(double x)
{
	if (x > 0 && x < 10)
		water = x;
	else
		x = 5.0;
}

int Environment::get_field()
{
	return field;
}

double Environment::get_water()
{
	return water;
}

