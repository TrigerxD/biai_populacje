#include "../HeaderFiles/Calendar.h"

Calendar::Calendar() {
	this->day = 0;
	this->month = 0;
	this->year = 0;
}

Calendar::Calendar(int t_day, int t_month, int t_year) {
	if (Calendar::validate(t_day, t_month, t_year)) {
		this->day = t_day;
		this->month = t_day;
		this->year = t_year;
	}
	else {
		this->day = 0;
		this->month = 0;
		this->year = 0;
	}
}

Calendar::Calendar(std::string date) {
	std::string construtVal[3];
	int a = 0;
	for (int i = 0; i < date.length(); i++) {
		if (date[i] == '-')
			a++;
		else
			construtVal[a] += date[i];
	}
	if (Calendar::validate(stoi(construtVal[2]), stoi(construtVal[1]), stoi(construtVal[0]))) {
		this->day = stoi(construtVal[2]);
		this->month = stoi(construtVal[1]);
		this->year = stoi(construtVal[0]);
	}
	else {
		this->day = 0;
		this->month = 0;
		this->year = 0;
	}
}

Calendar::Calendar(const Calendar &date) {
	this->day = date.day;
	this->month = date.month;
	this->year = date.year;
}

bool Calendar::validate(int t_day, int t_month, int t_year) {
	//if (t_year > 0) {
	//	if ((t_year % 4 == 0) && (t_year % 100 != 0) || t_year % 400 == 0) {
	//		if (t_month > 0 && t_month < 13) {
	//			day_month[2]++;
	//			if (t_day > 0 && t_day <= day_month[t_month - 1]) {
	//				day_month[2]--;
	//				return true;
	//			}
	//		}
	//	}
	//	else {
	//		if (t_month > 0 && t_month < 13) {
	//			if (t_day > 0 && t_day <= day_month[t_month - 1])
	//				return true;
	//		}
	//	}
	//}
	//return false;
	return true;
}

std::string Calendar::get_date() {
	return (std::to_string(day) + "-" + std::to_string(month) + "-" + std::to_string(year));
}

void Calendar::set_day(int new_day) {
	day = new_day;
}

void Calendar::set_month(int new_month) {
	month = new_month;
}

void Calendar::set_year(int new_year) {
	year = new_year;
}

int Calendar::get_day() {
	return day;
}

int Calendar::get_month() {
	return month;
}

int Calendar::get_year() {
	return year;
}

int Calendar::month_sum(int last_month) {
	int retVal = 0;
	for (int i = 0; i < last_month; i++)
		retVal += day_month[i];
	return retVal;
}

int Calendar::get_day_month(int index)
{
	return day_month[index];
}

bool Calendar::operator==(Calendar& x) {
	if ((this->get_day() == x.get_day()) && (this->get_month() == x.get_month()) && (this->get_year()== x.get_year()))
		return true;
	return false;
}
Calendar * Calendar::operator=(Calendar& x) {
	this->set_day(x.get_day());
	this->set_month(x.get_month());
	this->set_year(x.get_year());
	return this;
}