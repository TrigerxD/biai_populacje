#include "../HeaderFiles/Simulation.h"

Simulation::Simulation() {

}

float dependency_at_temperature(float temperature, Species * fellow) {
	float retVal = 0.0f;
	retVal += (-temperature)*(fellow->get_weight() - 5) / 100;
	retVal += ((pow(temperature, 2) - 15) + fellow->get_condition() + fellow->get_durability()) / 100;
	retVal += (-temperature)*(fellow->get_fur() - 5) / 100;							
	retVal += (temperature)*(fellow->get_food_demand() - 5) / 100;
	retVal /= fellow->get_temp_resistance();
	return retVal;
}

float dependency_at_wind_speed(float wind_speed, Species * fellow) {
	float retVal = 0.0f;
	retVal += (wind_speed)*(10 - fellow->get_size() - fellow->get_weight()) / 100;
	retVal += wind_speed * fellow->get_speed() / 100;
	if (fellow->get_flight())
		retVal /= 2;
	if (fellow->get_enviroment()==2)
		retVal -= wind_speed / 100;
	return retVal;
}

float dependency_at_cloudiness(float cloudiness, Species * fellow) {
	float retVal = 0.0f;
	retVal += (cloudiness) * (5 - fellow->get_speed()) / 100;
	if (fellow->get_flight())
		retVal /= 2;
	return retVal;
}

float dependency_at_fall(float fall, Species * fellow) {
	float retVal = 0.0f;
	retVal += (fall) * (5 - fellow->get_speed()) / 100;
	retVal += (fall) * (7 - fellow->get_durability()) / 100;
	retVal += (fall) * (3 - fellow->get_temp_resistance()) / 100;
	retVal -= (fall) * (fellow->get_fur()) / 100;
	if (fellow->get_flight())
		retVal /= 2;
	if (fellow->get_enviroment() == 1)
		retVal *= 0;
	return retVal;
}

float dependency_at_pressure(float pressure, Species * fellow) {
	float retVal = 0.0f;
	retVal += (pressure) * (5 -fellow->get_size() - fellow->get_weight() + fellow->get_speed() + fellow->get_durability()) / 10000;
	if (fellow->get_flight())
		retVal /= 2;
	return retVal;
}

float dependency_at_humididty(float humidity, Species * fellow) {
	float retVal = 0.0f;
	retVal += (humidity) * (5 -fellow->get_weight() + fellow->get_speed() + fellow->get_temp_resistance() - fellow->get_fur() - fellow->get_food_demand()) / 1000;
	if (fellow->get_flight())
		retVal -= 0.01f;
	if (fellow->get_enviroment() == 1)
		retVal *= 0;
	return retVal;
}

float dependency_at_snow_height(float snow_height, Species * fellow) {
	float retVal = 0.0f;
	retVal += (snow_height) * (-fellow->get_size() - fellow->get_weight() + fellow->get_speed() + fellow->get_condition() + fellow->get_durability() + fellow->get_temp_resistance() - fellow->get_fur() * 2) / 100;
	return retVal;
}

void task(Environment * env, Data params, Population &pop, Calendar t_date, int size, int start) {
	float temperature = params.get_average_param(params_types::temperature, t_date);
	float cloudiness = params.get_average_param(params_types::cloudiness, t_date);
	float fall = params.get_average_param(params_types::fall, t_date);
	float wind_speed = params.get_average_param(params_types::wind_speed, t_date);
	float humidity = params.get_average_param(params_types::humidity, t_date);
	float pressure = params.get_average_param(params_types::pressure, t_date);
	float snow_height = params.get_average_param(params_types::snow_height, t_date);
	for (int i = start; i < start + size; i++) {
		float retVal = 0;
		retVal += dependency_at_temperature(temperature, &pop.getPopulation()[i]);
		retVal += dependency_at_cloudiness(cloudiness, &pop.getPopulation()[i]);
		retVal += dependency_at_fall(fall, &pop.getPopulation()[i]);
		retVal += dependency_at_wind_speed(wind_speed, &pop.getPopulation()[i]);
		retVal += dependency_at_humididty(humidity, &pop.getPopulation()[i]);
		retVal += dependency_at_pressure(pressure, &pop.getPopulation()[i]);
		retVal += dependency_at_snow_height(snow_height, &pop.getPopulation()[i]);
		pop.set_nominal_value(i, (retVal + pop.getPopulation()[i].get_nominal_value()) / 2);
	}
}

void Simulation::nominal_value_func(Environment * env, Data params, Population &pop, Calendar t_date, Species * pattern) {
	srand(time(NULL));
	int i_threads = pop.getPopulation().size() / 100;
	float retVal = 0;
	int size = pop.getPopulation().size() / i_threads;

	retVal += dependency_at_temperature(params.get_average_param(params_types::temperature, t_date), pattern);
	retVal += dependency_at_cloudiness(params.get_average_param(params_types::cloudiness, t_date), pattern);
	retVal += dependency_at_fall(params.get_average_param(params_types::fall, t_date), pattern);
	retVal += dependency_at_wind_speed(params.get_average_param(params_types::wind_speed, t_date), pattern);
	retVal += dependency_at_humididty(params.get_average_param(params_types::humidity, t_date), pattern);
	retVal += dependency_at_pressure(params.get_average_param(params_types::pressure, t_date), pattern);
	retVal += dependency_at_snow_height(params.get_average_param(params_types::snow_height, t_date), pattern);
	pattern->set_nominal_value((retVal + pattern->get_nominal_value()) / 2);

	std::vector<std::thread> threads;
	for (int i = 0; i < i_threads;  i++) {
		std::thread t(&task, env, params, std::ref(pop), t_date, size, i * size);
		threads.push_back(std::move(t));
	}
	for (int i = 0; i < threads.size(); i++) {
		threads[i].join();
	}
	//TODO
}

void Simulation::mutate(Species * fellow) {
	fellow->mutate();
}

void Simulation::evolve(Species * fellow) {
	fellow->evolving();
}

bool Simulation::is_death_after_disaster(Population * pop)
{
	std::vector<Species> fellow = pop->getPopulation();
	srand(time(NULL));
	double chance = rand() % 10000 / 10000.0;
	int cat = rand() % 5;
	if ((chance >= 0.002 && chance <= 0.008) || (chance >= 0.046 && chance <= 0.054) || (chance >= 0.092 && chance <= 0.098)) //chance for earthquake
	{
		std::fstream out;
		out.open("report.txt", std::ios::out | std::ios::app);
		if (out.good()) {
			switch (cat) {
			case 0:
					out << "Earthquake, land species will extinct" << std::endl;
					pop->death_by_cataclism(durability, 0);
					pop->death_by_cataclism(condition, 0);
				break;
			case 1:
					out << "Drought, all species will extinct" << std::endl;
					pop->death_by_cataclism(temp_resistance, 0);
					pop->death_by_cataclism(food_demand, 0);
					pop->death_by_cataclism(temp_resistance, 1);
					pop->death_by_cataclism(food_demand, 1);
					pop->death_by_cataclism(temp_resistance, 2);
					pop->death_by_cataclism(food_demand, 2);
				break;
			case 2:
					out << "Cyclone, land and air species will extinct" << std::endl;
					pop->death_by_cataclism(size, 0);
					pop->death_by_cataclism(weight, 0);
					pop->death_by_cataclism(condition, 0);
					pop->death_by_cataclism(size, 1);
					pop->death_by_cataclism(weight, 1);
					pop->death_by_cataclism(condition, 1);
				break;
			case 3:
					out << "Volcano erruption, land and air species will extinct" << std::endl;
					pop->death_by_cataclism(durability, 0);
					pop->death_by_cataclism(temp_resistance, 0);
					pop->death_by_cataclism(condition, 0);
					pop->death_by_cataclism(durability, 1);
					pop->death_by_cataclism(condition, 1);
					pop->death_by_cataclism(food_demand, 1);
				break;
			case 4:
					out << "Flood, land species will extinct" << std::endl;
					pop->death_by_cataclism(size, 0);
					pop->death_by_cataclism(weight, 0);
					pop->death_by_cataclism(condition, 0);
				break;
			default:
				break;
			}
		}
		out.close();
	}
	else //chance for no disaster
	{
		std::cout << "No disaster, all species will survive" << std::endl;
		return false;
	}
	return true;
}


void Simulation::death(Population * pop, float nominal_value) {
	pop->sort();
	int how_many = 0;
	for (int i = 0; i < pop->getPopulation().size(); i++) {
		if (pop->getPopulation()[i].get_nominal_value() <= nominal_value)
			++how_many;
	}
	pop->death(how_many);
}

Species * Simulation::reproduce(Species * male, Species * female) {
	if (male->get_gender() == 0 && female->get_gender() == 1 && male->get_enviroment() == female->get_enviroment()) {
		return male->reproduce(female);
	}
	return nullptr;
}
