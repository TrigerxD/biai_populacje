#include "../HeaderFiles/Species.h"

Species::Species()
{
	this->size = 0;
	this->weight = 0;
	this->speed = 0;
	this->durability = 0;
	this->condition = 0;
	this->temp_resistance = 0;
	this->enviroment = 0;
	this->food_demand = 0;
	this->food_type = 0;
	this->fur = 0;
	this->lifetime = 0;
	this->flight = false;
	this->gender = false;
	this->evolve = false;
	nominal_value = -1;
}

Species::Species(const Species &tmp)
{
	this->size = tmp.size;
	this->weight = tmp.weight;
	this->speed = tmp.speed;
	this->durability = tmp.durability;
	this->condition = tmp.condition;
	this->temp_resistance = tmp.temp_resistance;
	this->enviroment = tmp.enviroment;
	this->food_demand = tmp.food_demand;
	this->food_type = tmp.food_type;
	this->fur = tmp.fur;
	this->flight = tmp.flight;
	this->gender = tmp.gender;
	this->evolve = tmp.evolve;
	this->nominal_value = tmp.nominal_value;
	this->lifetime = 0;
}

Species::Species(float * params)
{
	this->size = params[param_types::size];
	this->weight = params[param_types::weight];
	this->speed = params[param_types::speed];
	this->durability = params[param_types::durability];
	this->condition = params[param_types::condition];
	this->temp_resistance = params[param_types::temp_resistance];
	this->enviroment = params[param_types::enviroment];
	this->food_demand = params[param_types::food_demand];
	this->food_type = params[param_types::food_type];
	this->fur = params[param_types::fur];
	this->flight = params[param_types::flight];
	this->gender = params[param_types::gender];
	this->evolve = false;
	this->lifetime = 0;
	this->nominal_value = -1;
}

void Species::add_parameters(Species* obj)
{
	obj->size = size;
	obj->weight = weight;
	obj->speed = speed;
	obj->durability = durability;
	obj->condition = condition;
	obj->temp_resistance = temp_resistance;
	obj->enviroment = enviroment;
	obj->food_demand = food_demand;
	obj->food_type = food_type;
	obj->fur = fur;
	obj->flight = flight;
	obj->gender = gender;
	obj->lifetime = 0;
	obj->nominal_value = -1;
}

void Species::mutate() {
	srand(time(NULL));
	this->speed = (rand() % MAX_GROW + MIN_VALUE);
	this->durability = (rand() % MAX_GROW + MIN_VALUE);
	this->condition = (rand() % MAX_GROW + MIN_VALUE);
	this->temp_resistance = (rand() % MAX_GROW + MIN_VALUE);
	this->fur = (rand() % MAX_GROW + MIN_VALUE);
	this->food_demand = (rand() % MAX_GROW + MIN_VALUE);
}

void Species::evolving() {
	if ((this->speed += EVOLVE) > MAX_EVOLVE)
		this->speed = MAX_EVOLVE;
	if ((this->durability += EVOLVE) > MAX_EVOLVE)
		this->durability = MAX_EVOLVE;
	if ((this->condition += EVOLVE) > MAX_EVOLVE)
		this->condition = MAX_EVOLVE;
	if ((this->temp_resistance += EVOLVE) > MAX_EVOLVE)
		this->temp_resistance = MAX_EVOLVE;
	if ((this->food_demand -= EVOLVE) < MIN_VALUE)
		this->food_demand = MIN_VALUE;
	this->evolve = true;
}

void Species::grow() {
	if ((this->size += MODIFICATE) > MAX_GROW)
		this->size = MAX_GROW;
	if ((this->weight += MODIFICATE) > MAX_GROW)
		this->weight = MAX_GROW;
	if ((this->fur += MODIFICATE) > MAX_GROW)
		this->fur = MAX_GROW;
	if ((this->food_demand += MODIFICATE) > MAX_GROW)
		this->food_demand = MAX_GROW;
	if (this->weight > 5 && !this->evolve)
		this->speed = 5;
	else if (!this->evolve)
		(this->speed += MODIFICATE);
	this->lifetime++;
}

Species * Species::reproduce(Species * female) {
	float tmp[12];
	srand(time(NULL));
	double ratio = (rand() % 50 + 1) / 100;
	tmp[param_types::size] = 1;
	tmp[param_types::weight] = 1;
	tmp[param_types::speed] = 1;
	tmp[param_types::durability] = this->durability * (1.0 - ratio) + female->get_durability() * ratio;
	tmp[param_types::condition] = this->condition * (1.0 - ratio) + female->get_condition() * ratio;
	tmp[param_types::temp_resistance] = this->temp_resistance * (1.0 - ratio) + female->get_temp_resistance() * ratio;
	tmp[param_types::food_demand] = 2;
	tmp[param_types::food_type] = this->food_type;
	tmp[param_types::enviroment] = this->enviroment;
	tmp[param_types::fur] = 0;
	tmp[param_types::flight] = this->flight;
	if(rand()%2 == 1)
		tmp[param_types::gender] = true;
	else
		tmp[param_types::gender] = false;
	return new Species(tmp);
}

void Species::set_nominal_value(float x)
{
	this->nominal_value = x;
}

float Species::get_nominal_value()
{
	return nominal_value;
}

void Species::set_size(double new_size)
{
	size = new_size;
}
void Species::set_weight(double new_weight)
{
	weight = new_weight;
}
void Species::set_speed(double new_speed)
{
	speed = new_speed;
}
void Species::set_durability(double new_durability)
{
	durability = new_durability;
}
void Species::set_condition(double new_condition)
{
	condition = new_condition;
}
void Species::set_temp_resistance(double new_temp_resistance)
{
	temp_resistance = new_temp_resistance;
}
void Species::set_enviroment(double new_enviroment)
{
	enviroment = new_enviroment;
}
void Species::set_food_demand(double new_food_demand)
{
	food_demand = new_food_demand;
}
void Species::set_food_type(double new_food_type)
{
	food_type = new_food_type;
}
void Species::set_fur(double new_fur)
{
	fur = new_fur;
}
void Species::set_flight(bool new_flight)
{
	flight = new_flight;
}
void Species::set_gender(bool new_gender)
{
	gender = new_gender;
}
void Species::increment_params()
{
	size -= MODIFICATE;
	weight -= MODIFICATE;
	speed += MODIFICATE;
	durability += MODIFICATE;
	condition += MODIFICATE;
	temp_resistance += MODIFICATE;
	food_demand += MODIFICATE;
}

double Species::get_size()
{
	return size;
}
double Species::get_weight()
{
	return weight;
}
double Species::get_speed()
{
	return speed;
}
double Species::get_durability()
{
	return durability;
}
double Species::get_condition()
{
	return condition;
}
double Species::get_temp_resistance()
{
	return temp_resistance;
}
double Species::get_enviroment()
{
	return enviroment;
}
double Species::get_food_demand()
{
	return food_demand;
}
double Species::get_food_type()
{
	return food_type;
}
double Species::get_fur()
{
	return fur;
}
bool Species::get_flight()
{
	return flight;
}
bool Species::get_gender()
{
	return gender;
}

int Species::get_lifetime()
{
	return lifetime;
}
	