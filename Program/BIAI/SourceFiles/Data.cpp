#include "../HeaderFiles/Data.h"

Data::Data() {
	sortingVal = 0;
	_params = 0;
	actual_param = 0;
}

Data::Data(Calendar t_date) {
	first.push_back(alloc_params(t_date));
	first[0].date = Calendar(t_date);
	sortingVal = first[0].date.get_day() + first[0].date.month_sum(first[0].date.get_month() - 1);
	_params = 1;
}

Data::Data(const Data &t_date) {
	this->sortingVal = t_date.sortingVal;
	this->first = t_date.first;
	this->_params = t_date._params;
}

params Data::alloc_params(Calendar t_date) {
	params *_new = new params;
	_new->date = Calendar(t_date);
	_new->temperature = not_measure_value;
	_new->humidity = not_measure_value;
	_new->cloudiness = not_measure_value;
	_new->wind_speed = not_measure_value;
	_new->pressure = not_measure_value;
	_new->fall = not_measure_value;
	_new->snow_height = not_measure_value;
	return *_new;
}
void Data::add(Calendar t_date) {
	first.push_back(alloc_params(t_date));
	_params++;
}

int Data::add(params_types type, double value, Calendar t_date) {
	bool test = true;
	for(int i = actual_param; i < first.size(); i++, actual_param++) {
		switch (type) {
		case temperature:
			if (first[i].temperature == not_measure_value && first[i].date == t_date) {
				first[i].temperature = value;
				return 1;
			}
			else if(i+1 < first.size())
				if (first[i+1].temperature != not_measure_value && first[i+1].date == t_date)
					test = false;
			break;
		case humidity:
			if (first[i].humidity == not_measure_value && first[i].date == t_date) {
				first[i].humidity = value;
				return 1;
			}
			else if (i + 1 < first.size())
				if (first[i+1].humidity != not_measure_value && first[i+1].date == t_date)
					test = false;
			break;
		case cloudiness:
			if (first[i].cloudiness == not_measure_value && first[i].date == t_date) {
				first[i].cloudiness = value;
				return 1;
			}
			else if (i + 1 < first.size())
				if (first[i+1].cloudiness != not_measure_value && first[i+1].date == t_date)
					test = false;
			break;
		case wind_speed:
			if (first[i].wind_speed == not_measure_value && first[i].date == t_date) {
				first[i].wind_speed = value;
				return 1;
			}
			else if (i + 1 < first.size())
				if (first[i+1].wind_speed != not_measure_value && first[i+1].date == t_date)
					test = false;
			break;
		case pressure:
			if (first[i].pressure == not_measure_value && first[i].date == t_date) {
				first[i].pressure = value;
				return 1;
			}
			else if (i + 1 < first.size())
				if (first[i+1].pressure != not_measure_value && first[i+1].date == t_date)
					test = false;
			break;
		case fall:
			if (first[i].fall == not_measure_value && first[i].date == t_date) {
				first[i].fall = value;
				return 1;
			}
			else if (i + 1 < first.size())
				if (first[i+1].fall != not_measure_value && first[i+1].date == t_date)
					test = false;
			break;
		case snow_height:
			if (first[i].snow_height == not_measure_value && first[i].date == t_date) {
				first[i].snow_height = value;
				return 1;
			}
			else if (i + 1 < first.size())
				if (first[i+1].snow_height != not_measure_value && first[i+1].date == t_date)
					test = false;
			break;
		default:
			return 0;
		}
		if (!test)
			break;
	}

	return 0;
}

Calendar Data::get_date(int index) {
	return first[index].date;
}

int Data::get_params_number() {
	return _params;
}

double Data::get_average_param(params_types type, Calendar t_date) {
	double average = 0;
	double elements = _params;

	switch (type) {
	case temperature:
		for(int i = 0; i < first.size(); i++) {
			if ((first[i].temperature != not_measure_value) && first[i].date == t_date) {
				average += first[i].temperature;
			}
			else
				elements--;
		}
		break;
	case humidity:
		for(int i = 0; i < first.size(); i++) {
			if ((first[i].humidity != 0 && first[i].humidity != not_measure_value) && first[i].date == t_date) {
				average += first[i].humidity;
			}
			else
				elements--;
		} 
		break;
	case cloudiness:
		for(int i = 0; i < first.size(); i++) {
			if ((first[i].cloudiness != not_measure_value) && first[i].date == t_date) {
				average += first[i].cloudiness;
			}
			else
				elements--;			
		} 
		break;
	case wind_speed:
		for(int i = 0; i < first.size(); i++) {
			if ((first[i].wind_speed != not_measure_value) && first[i].date == t_date) {
				average += first[i].wind_speed;
			}
			else
				elements--;			
		} 
		break;
	case pressure:
		for(int i = 0; i < first.size(); i++) {
			if ((first[i].pressure != 0 && first[i].pressure != not_measure_value) && first[i].date == t_date) {
				average += first[i].pressure;
			}
			else
				elements--;			
		} 
		break;
	case fall:
		for(int i = 0; i < first.size(); i++) {
			if ((first[i].fall != not_measure_value) && first[i].date == t_date) {
				average += first[i].fall;
			}
			else
				elements--;			
		} 
		break;
	case snow_height:
		for(int i = 0; i < first.size(); i++) {
			if ((first[i].snow_height != not_measure_value) && first[i].date == t_date) {
				average += first[i].snow_height;
			}
			else
				elements--;			
		} 
		break;
	default:
		break;
	}

	if (elements == 0)
		return 0;
	return average/elements;
}

int Data::get_sortingVal() {
	return sortingVal;
}

void Data::sort() {
	for (int i = 0; i < first.size(); i++) {
		for (int j = i; j < first.size(); j++) {
			if (first[i].date.get_year() > first[j].date.get_year()) {
				params tmp = first[i];
				first[i] = first[j];
				first[j] = tmp;
			}
		}
	}
}

bool Data::is_first() {
	if (first.size() == 0)
		return true;
	return false;
}

void Data::set_actual_param()
{
	actual_param = 0;
}

Data * Data::operator=(Data & x) {
	this->first = x.first;
	this->_params = x._params;
	this->sortingVal = x.sortingVal;
	return this;
}
