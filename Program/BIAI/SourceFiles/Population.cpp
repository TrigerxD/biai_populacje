﻿#include "../HeaderFiles/Population.h"
#include <fstream>

Population::Population()
{
}

void Population::read_file(std::string file_name) {
	std::fstream input;
	input.open(file_name);
	if (input.good()) {
		std::string param = "";
		getline(input, param);
		int i = 0;
		clock_t start = ((clock()) / CLOCKS_PER_SEC);
		while (1) {
			getline(input, param);
			if (str_to_table(param)[0] == "end")
				break;
			add_person(str_to_table(param));
			i++;
		}
		std::cout << i << std::endl;
		std::cout << (clock() / CLOCKS_PER_SEC) - start << " s" << std::endl;
	}
	input.close();
}

void Population::death(int how_many) {
	for (int i = 0; i < how_many; i++)
		parameters.pop_back();
}

void Population::death_by_cataclism(param_types type, int env)
{
	sort_by_param(type, env);
	int how_many = 0;
	switch (type)
	{
	case size:
		for (int i = 0; i < parameters.size(); i++) {
			if (parameters[i].get_size() <= 7 && parameters[i].get_enviroment() == env)
				++how_many;
		}
		death(how_many);
		break;
	case weight:
		for (int i = 0; i < parameters.size(); i++) {
			if (parameters[i].get_weight() <= 7)
				++how_many;
		}
		death(how_many);
		break;
	case speed:
		for (int i = 0; i < parameters.size(); i++) {
			if (parameters[i].get_speed() <= 7)
				++how_many;
		}
		death(how_many);
		break;
	case durability:
		for (int i = 0; i < parameters.size(); i++) {
			if (parameters[i].get_durability() <= 7)
				++how_many;
		}
		death(how_many);
		break;
	case condition:
		for (int i = 0; i < parameters.size(); i++) {
			if (parameters[i].get_condition() <= 7)
				++how_many;
		}
		death(how_many);
		break;
	case temp_resistance:
		for (int i = 0; i < parameters.size(); i++) {
			if (parameters[i].get_temp_resistance() <= 7)
				++how_many;
		}
		death(how_many);
		break;
	case enviroment:
		return;
	case food_demand:
		for (int i = 0; i < parameters.size(); i++) {
			if (parameters[i].get_food_demand() <= 7)
				++how_many;
		}
		death(how_many);
		break;
	case food_type:
		return;
	case fur:
		for (int i = 0; i < parameters.size(); i++) {
			if (parameters[i].get_fur() <= 7)
				++how_many;
		}
		death(how_many);
		break;
	case flight:
		return;
	case gender:
		return;
	default:
		return;
	}
}

void Population::sort() {
	for (int i = 0; i < parameters.size(); i++) {
		for (int j = i; j < parameters.size(); j++) {
			if (parameters[j].get_lifetime() == 20)
				parameters[j].set_nominal_value(-999);
			if (parameters[i].get_nominal_value() < parameters[j].get_nominal_value()) {
				Species tmp(parameters[i]);
				parameters[i] = parameters[j];
				parameters[j] = tmp;
			}
		}
	}
}

void Population::sort_by_param(param_types type, int env)
{
	for (int i = 0; i < parameters.size(); i++) {
		for (int j = i; j < parameters.size(); j++) {
			switch (env) {
			case 0:
				if (parameters[i].get_enviroment() < parameters[j].get_enviroment()) {
					Species tmp(parameters[i]);
					parameters[i] = parameters[j];
					parameters[j] = tmp;
				}
				break;
			case 1:
				if (parameters[i].get_enviroment() == env && parameters[j].get_enviroment() != env) {
					Species tmp(parameters[i]);
					parameters[i] = parameters[j];
					parameters[j] = tmp;
				}
				break;
			case 2:
				if (parameters[i].get_enviroment() > parameters[j].get_enviroment()) {
					Species tmp(parameters[i]);
					parameters[i] = parameters[j];
					parameters[j] = tmp;
				}
				break;
			default:
				return;
			}
		}
	}
	for (int i = 0; i < parameters.size(); i++) {
		for (int j = i; j < parameters.size() && parameters[i].get_enviroment() == env; j++) {
			int p1 = 0, p2 = 0;
			switch (type)
			{
			case size:
				p1 = parameters[i].get_size();
				p2 = parameters[j].get_size();
				break;
			case weight:
				p1 = parameters[i].get_weight();
				p2 = parameters[j].get_weight();
				break;
			case speed:
				p1 = parameters[i].get_speed();
				p2 = parameters[j].get_speed();
				break;
			case durability:
				p1 = parameters[i].get_durability();
				p2 = parameters[j].get_durability();
				break;
			case condition:
				p1 = parameters[i].get_condition();
				p2 = parameters[j].get_condition();
				break;
			case temp_resistance:
				p1 = parameters[i].get_temp_resistance();
				p2 = parameters[j].get_temp_resistance();
				break;
			case enviroment:
				return;
			case food_demand:
				p1 = parameters[i].get_food_demand();
				p2 = parameters[j].get_food_demand();
				break;
			case food_type:
				return;
			case fur:
				p1 = parameters[i].get_fur();
				p2 = parameters[j].get_fur();
				break;
			case flight:
				return;
			case gender:
				return;
			default:
				return;
			}
			if (p1 < p2) {
				Species tmp(parameters[i]);
				parameters[i] = parameters[j];
				parameters[j] = tmp;
			}
		}
	}
}

void Population::set_nominal_value(int index, float value)
{
	parameters[index].set_nominal_value(value);
}

void Population::add(Species * fellow)
{
	parameters.push_back(*fellow);
}

void Population::get_average()
{
	double tab[9] = { 0 };
	for (int i = 0; i < parameters.size(); i++) {
		tab[0] += parameters[i].get_size();
		tab[1] += parameters[i].get_weight();
		tab[2] += parameters[i].get_speed();
		tab[3] += parameters[i].get_durability();
		tab[4] += parameters[i].get_condition();
		tab[5] += parameters[i].get_temp_resistance();
		tab[6] += parameters[i].get_food_demand();
		tab[7] += parameters[i].get_fur();
		tab[8] += parameters[i].get_lifetime();
	}
	tab[0] /= parameters.size();
	tab[1] /= parameters.size();
	tab[2] /= parameters.size();
	tab[3] /= parameters.size();
	tab[4] /= parameters.size();
	tab[5] /= parameters.size();
	tab[6] /= parameters.size();
	tab[7] /= parameters.size();
	tab[8] /= parameters.size();

	std::fstream out;
	out.open("report.txt", std::ios::out | std::ios::app);
	if (out.good()) {
		out << "Average population params" << std::endl;
		out << "Size : " << tab[0] << std::endl;
		out << "Weight : " << tab[1] << std::endl;
		out << "Speed : " << tab[2] << std::endl;
		out << "Durability : " << tab[3] << std::endl;
		out << "Condition : " << tab[4] << std::endl;
		out << "Temp resistance : " << tab[5] << std::endl;
		out << "Food demand : " << tab[6] << std::endl;
		out << "Fur : " << tab[7] << std::endl;
		out << "Lifetime : " << tab[8] << std::endl;
	}
	out.close();
}

void Population::grow()
{
	for (int i = 0; i < parameters.size(); i++)
		parameters[i].grow();
}

std::string * Population::str_to_table(std::string x)
{
	std::string * retVal = new std::string[find_all(x, ';') + 1];
	int a = 0;
	for (int i = 0; i < x.length(); i++) {
		if (x[i] == ';')
			a++;
		else if (x[i] == ',')
			retVal[a] += '.';
		else
			retVal[a] += x[i];
	}
	return retVal; //zwraca tabelę wszystkich elemetów (wiecej o 1 niz liczba sredników)
}

void Population::add_person(std::string * params)
{
	float retVal[12];
	retVal[param_types::size] = atof(params[param_types::size].c_str());
	retVal[param_types::weight] = atof(params[param_types::weight].c_str());
	retVal[param_types::speed] = atof(params[param_types::speed].c_str());
	retVal[param_types::durability] = atof(params[param_types::durability].c_str());
	retVal[param_types::condition] = atof(params[param_types::condition].c_str());
	retVal[param_types::temp_resistance] = atof(params[param_types::temp_resistance].c_str());
	retVal[param_types::enviroment] = atof(params[param_types::enviroment].c_str());
	retVal[param_types::food_demand] = atof(params[param_types::food_demand].c_str());
	retVal[param_types::food_type] = atof(params[param_types::food_type].c_str());
	retVal[param_types::fur] = atof(params[param_types::fur].c_str());
	retVal[param_types::flight] = atoi(params[param_types::flight].c_str());
	retVal[param_types::gender] = atoi(params[param_types::gender].c_str());

	parameters.push_back(Species(retVal));
}

std::vector<Species> Population::getPopulation()
{
	return parameters;
}



