#include "../HeaderFiles/Source.h"

void main_loop(int period, Environment * env, Population * pop) {
	srand(time(NULL));
	std::cout << "Let's start simulation\n" << std::endl;
	float changing_param = 1.0f;
	Simulation * sim = new Simulation();
	// funkcja losowych kataklizmów
	// ustawienie parametrów stałych
	int year_start = 1950;
	Calendar start(1, 1, 1950);
	float params[12] = { 5,5,5,5,5,5,0,5,1,0,0,0 };
	Species * pattern = new Species(params);
	int modificate_pattern = 0;
	std::fstream t("report.txt", std::ios::out | std::ios::trunc);
	if (t.good())
		t.close();
	do {
		if (changing_param == 0) {
			break;
		}
		else if (changing_param >= 1) {
			changing_param = 1;
		}
		else if (changing_param <= -1) {
			changing_param = -1;
		}
		
		for (int x = 0; x < period; x++) {
			if (pop->getPopulation().size() == 0) {
				std::cout << "Whole population dead xD" << std::endl;
				return;
			}

			int reborn = 0, evolve = 0, death = 0, mutate = 0;
			for (int i = 0; i < YEAR; i++) {
				std::cout << start.get_date() << std::endl;
				sim->nominal_value_func(env, env->get_parameters()[i], *pop, start, pattern);
				start.set_day(start.get_day() + 1);
				if (start.get_day() > start.get_day_month(start.get_month() - 1)) {
					start.set_day(1);
					start.set_month(start.get_month() + 1);
				}
			}
			start.set_day(1);
			start.set_month(1);
			start.set_year(start.get_year() + 1);

			pop->grow();

			death = pop->getPopulation().size();
			sim->death(pop, pattern->get_nominal_value());
			sim->is_death_after_disaster(pop);
			death = death - pop->getPopulation().size();
			int k = 0;
			for (int i = 0; i < pop->getPopulation().size(); i++) {
				if (!pop->getPopulation()[i].get_gender()) {
					while (true) {
						if (pop->getPopulation()[k].get_gender()) {
							Species * tmp = sim->reproduce(&pop->getPopulation()[i], &pop->getPopulation()[k]);
							double t_mutate = rand() % 10000 / 10000.0;
							t_mutate *= (pow(10, ceil(log10(pop->getPopulation().size()))) - pop->getPopulation().size())/ pow(10,ceil(log10(pop->getPopulation().size())));
							double t_evolution = rand() % 10000 / 10000.0;
							if (t_mutate < 0.015) {
								sim->mutate(tmp);
								mutate++;
							}
							if (t_evolution > 0.49 - (ceil(log10(pop->getPopulation().size())) - pop->getPopulation().size()) / pow(10, ceil(log10(pop->getPopulation().size())) + 1) &&
								0.55 + (ceil(log10(pop->getPopulation().size())) - pop->getPopulation().size()) / pow(10, ceil(log10(pop->getPopulation().size())) + 1) > t_evolution) {
								sim->evolve(&pop->getPopulation()[i]);
								evolve++;
							}
							else if (t_evolution > 0.985 - (ceil(log10(pop->getPopulation().size())) - pop->getPopulation().size()) / pow(10, ceil(log10(pop->getPopulation().size())) + 1)) {
								sim->evolve(&pop->getPopulation()[k]);
								evolve++;
							}
							if (tmp != nullptr) {
								pop->add(tmp);
								reborn++;
								delete(tmp);
							}
							k++;
							break;
						}
						if (pop->getPopulation()[k].get_nominal_value() < (pop->getPopulation()[0].get_nominal_value())*0.5)
							goto AFTER;
						k++;
					}
				}
			}
		AFTER:
			if (modificate_pattern < 7 && death < reborn / 10) {
				pattern->increment_params();
				modificate_pattern++;
			}
			std::cout << "Population in a year " << start.get_year() -1 << "\n" << pop->getPopulation().size() << std::endl;
			std::cout << "Reborn : " << reborn << "  Death : " << death << "  Mutate : " << mutate << "  Evolve : " << evolve << std::endl;
			std::fstream out;
			out.open("report.txt", std::ios::out | std::ios::app);
			if (out.good()) {
				out << "\nPopulation in a year " << start.get_year() -1 << "\n" << pop->getPopulation().size() << std::endl;
				out << "Reborn : " << reborn << "  Death : " << death << "  Mutate : " << mutate << "  Evolve : " << evolve << std::endl;
			}
			out.close();
			pop->get_average();
			_sleep(5000);
			std::cout << std::endl;
		}
		start.set_year(year_start);
		std::cout << "Enter modifire for parameters from -1 to 1\n1 - no change; 0 - exit program" << std::endl;
	} while (std::cin >> changing_param);
}

int main() {
	Environment * env = new Environment();
	Population * pop = new Population();

	std::string file_name_2 = "F:\\Studia\\Studia_6sem\\BIAI\\biai_populacje\\Params\\TWC.csv";
	std::string file_name_3 = "F:\\Studia\\Studia_6sem\\BIAI\\biai_populacje\\Params\\HP.csv";
	std::string file_name_1 = "F:\\Studia\\Studia_6sem\\BIAI\\biai_populacje\\Params\\FS.csv";
	env->read_file(file_name_1);
	env->read_file(file_name_2);
	env->read_file(file_name_3);
	std::string file_name = "F:\\Studia\\Studia_6sem\\BIAI\\biai_populacje\\Params\\Populacje\\random.csv";
	pop->read_file(file_name);
	
	int period = 0;
	std::cout << "Enter period of one run simulation (10, 25 or 50 years)\nDefault value for period is 50\n";
	//std::cin >> period;
	if (period != 10 && period != 25)
		period = 50;
	std::cout << "Period : " << period << std::endl;
	
	std::string temp;
	std::cout << "Enter type of land where species live (mountainous, plain, beach)\nDefault is plain" << std::endl;
	/*std::cin >> temp;
	std::cout << "Land : " << temp << std::endl;
	env->set_field(temp);*/

	double tmp;
	std::cout << "Enter water accesibility in range 0 - 10 (double precision)\nDefault is 5" << std::endl;
	/*std::cin >> tmp;
	std::cout << "Water : " << tmp << std::endl;
	env->set_water(tmp);*/

	main_loop(period, env, pop);

	return 0;
}
